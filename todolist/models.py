from django.db import models

# Create your models here.
from django.contrib.auth.models import User

class TodoItem(models.Model):
    # CharField = varchar in mySQL
    task_name =  models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default='pending')
    date_created = models.DateTimeField('date created')
    user = models.ForeignKey(User, on_delete = models.RESTRICT, default = "")
    
class Event(models.Model):
    event_name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    status = models.CharField(max_length=50, default='pending')
    event_date = models.DateTimeField('date created')
    user = models.ForeignKey(User,on_delete= models.RESTRICT, default="")