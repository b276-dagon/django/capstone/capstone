from django.shortcuts import render , redirect , get_object_or_404

# Create your views here.

# The from keyword allows importing of necessary classes/modules and others files needed in our application from the django.http while the import keyword defines what we are importing from the package

from django.http import HttpResponse
from . models import TodoItem, Event

#to use the template created:
from django.template import loader

#import the build in User Model
from django.contrib.auth.models import User

#import the authenticate function
from django.contrib.auth import authenticate , login , logout

from django.forms.models import model_to_dict

#import forms 
from . forms import LoginForm, AddTaskForm ,UpdateTaskForm ,RegisterForm , AddEventForm , UpdateUserForm

from django.utils import timezone

from django.contrib.auth.hashers import make_password





def todoitem(request, todoitem_id):
    # response = f"You are viewing the details of {todoitem_id}"
    # return HttpResponse(response)
    # model_to_dict(TodoItem.objects.get(pk = todoitem_id))
    todoitem = get_object_or_404(TodoItem ,pk = todoitem_id)
    return render(request, "todoitem.html", model_to_dict(todoitem))



def index(request):
    
    todoitem_list = TodoItem.objects.filter(user_id = request.user.id)
    event_list = Event.objects.filter(user_id = request.user.id)
    user_list = User.objects.filter(id = request.user.id)
    #template = loader.get_template("index.html")
    context = {
        'todoitem_list' : todoitem_list,
        'event_list' : event_list,
        'user_list' : userlist
        # 'user' : request.user
    }
    #output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
    #return HttpResponse(template.render(context, request)) 
    # render (request, route_template, context)
    return render(request, "index.html", context)


def register(request):

    user = User()
    context = {}
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid() == False:
             form = RegisterForm()
        else:
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = make_password(form.cleaned_data['password'])

            duplicates = User.objects.filter(email = email)
            
           
            if not duplicates:
                User.objects.create(username = username, first_name = first_name,last_name = last_name,email = email, password = password)
                return redirect("todolist:index")
            else:
                context ={
                    'error' : True
                }
            
    return render(request, "register.html", context)
            
def change_password(request):
    is_user_authenticated = False

    user = authenticate(username = "johndoe", password = "john1234")

    

    context = {
    'is_user_authenticated' : is_user_authenticated
    }
    return render(request, "change_password.html", context)

def login_user(request):
    context = {

    }
    # if this is a post request we need to process the form data

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()
        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username = username, password = password)
            if user is not None:
                context = {
                    'username' : username,
                    'password' : password
                }
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    'error' : True
                }
           
    return render(request,"login.html", context)


    # username = 'johndoe'
    # password = 'johndoe12345'


    # user = authenticate(username = username , password = password)

    # if user is not None:
    #     login(request, user)
    #     return redirect("todolist:index")
    # else:
    #     return render(request,"login.html")

def logout_user(request):
    logout(request)
    return redirect("todolist:index")

def add_task(request):
    context = {}
    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            duplicates = TodoItem.objects.filter(task_name = task_name, user_id = request.user.id)

            if not duplicates:
                #create an object based on the TodoItem model and saves to the record in the database
                TodoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id)

                return redirect("todolist:index")
            else:
                context = { 
                    'error' : True
                }
    return render(request, "add_task.html", context)

def update_task(request, todoitem_id):
    todoitem = TodoItem.objects.filter(pk = todoitem_id)

    context = {
        'user' : request.user,
        'todoitem_id' : todoitem_id,
        'task_name' : todoitem[0].task_name,
        'description' :  todoitem[0].description,
        'status' :  todoitem[0].status


    }
    if request.method == "POST":
        form = UpdateTaskForm (request.POST)

        if form.is_valid() == False:
             form = UpdateTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()
                return redirect("todolist:viewtodoitem", todoitem_id=todoitem[0].id)
            else:

                context = {
                    'error' : True
                }

    return render(request, "update_task.html", context)


def delete_task(request, todoitem_id):
    TodoItem.objects.filter(pk = todoitem_id).delete()
    return redirect("todolist:index")

def eventlist(request, eventlist_id):

    eventlist = get_object_or_404(Event, pk= eventlist_id)
    return render(request, "eventlist.html", model_to_dict(eventlist))

def add_event(request):
    context = {}
    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if form.is_valid() == False:
            form = AddEventForm()
        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            duplicates = Event.objects.filter(event_name = event_name, user_id = request.user.id)

            if not duplicates:
                #create an object based on the TodoItem model and saves to the record in the database
                Event.objects.create(event_name = event_name, description = description, event_date = timezone.now(), user_id = request.user.id)

                return redirect("todolist:index")
            else:
                context = { 
                    'error' : True
                }
    return render(request, "add_event.html", context)

def delete_event(request, eventlist_id):
    Event.objects.filter(pk = eventlist_id).delete()
    return redirect("todolist:index")
            
def update_profile(request):

    

    context = {}
    success = False

    if request.method == "POST":

        user = User.objects.get(username = request.user.username)
        form = UpdateUserForm(request.POST)

        if form.is_valid() == False:
            form = UpdateUserForm()

        else:

            newpass = form.cleaned_data['password']

            user.set_password(newpass)
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']

            user.save()
            success = True

        context = {
            'success': success
        }


    return render(request, "update_profile.html", context)

def userlist(request,id):

    userlist = get_object_or_404(User ,pk = id)
    return render(request, "update_profile.html", model_to_dict(userlist))