# [Section] Models
# Each model is represented by a class that inherits the django.db.models.Model.
# Each model has a number of class variables , each of which represents a database field in the model.
# Each field is represented by an instance of a field class eg. charfield for character fields and DateTimeField for date times. This tells django what type of data each fields holds.
# Some field classes have required arguments. CharField requires that you give a max_length

# [Section] Migration
# python manage.py makemigrations todolist
# by running make migrations, you're telling django that you've made some changes to your models

#migrate
# python manage.py sqlmigrate todolist 0001
# python manage.py migrate


#python manage.py shell

#todoitem = TodoItem(task_name = "Eat", description = "Dinner", date_created = #timezone.now())

#save
#todoitem.save()

# for the mysql connection
#pip install mysql_connector
#pip install mysql

#creating superuser
#winpty python manage.py createsuperuser